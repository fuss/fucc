CLONEZILLA_SOURCE="https://downloads.sourceforge.net/project/clonezilla/clonezilla_live_stable/2.6.2-15/clonezilla-live-2.6.2-15-i686.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fclonezilla%2Ffiles%2Fclonezilla_live_stable%2F2.6.2-15%2Fclonezilla-live-2.6.2-15-i686.zip%2Fdownload%3Fuse_mirror%3Dnetix&ts=1568208151"
CLONEZILLA_SAVE_TO="../clonezilla-orig.zip"


all: 


full-clean:
	rm -f ${CLONEZILLA_SAVE_TO}
	rm -rf live


download:
	test -e ${CLONEZILLA_SAVE_TO} || wget -O ${CLONEZILLA_SAVE_TO} ${CLONEZILLA_SOURCE}


prepare-clonezilla: download
	test -e live || unzip ${CLONEZILLA_SAVE_TO} live/*
	rm -f live/filesystem.packages live/filesystem.packages-remove live/filesystem.size live/freedos.img live/ipxe.efi live/ipxe.lkn live/memtest


rebuild-squashfs: prepare-clonezilla
	useradd clonezilla || true
	cd live; test -e squashfs-root || unsquashfs filesystem.squashfs
	cd live/squashfs-root; test -e home/clonezilla/.ssh || mkdir -p home/clonezilla/.ssh
	cd live/squashfs-root/home/clonezilla/.ssh; test -e id_rsa || ssh-keygen -t rsa -N "" -f id_rsa -C "clonezilla@fucc-live"
	chmod 700 live/squashfs-root/home/clonezilla/.ssh
	chmod 600 live/squashfs-root/home/clonezilla/.ssh/id_rsa
	chmod 644 live/squashfs-root/home/clonezilla/.ssh/id_rsa.pub
	chown -R clonezilla. live/squashfs-root/home/clonezilla/.ssh
	cd live; rm -f filesystem.squashfs; mksquashfs squashfs-root filesystem.squashfs -comp zstd
	rm -f live/squashfs-root/home/clonezilla/.ssh/id_rsa
	cp live/squashfs-root/home/clonezilla/.ssh/id_rsa.pub utils/clonezilla_live.pub


no-rebuild-squashfs: download
	test -e live || unzip ${CLONEZILLA_SAVE_TO} live/*
	echo "NO KEY HERE - BUSTER BUILD" > utils/clonezilla_live.pub
