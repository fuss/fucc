# FUSS-FUCC - Fully Unattended Clonezilla Cloning

##  Installazione di FUSS-FUCC 

`fuss-fucc` è presente nel repository `archive.fuss.bz.it` di FUSS, pertanto è sufficiente installarlo con `apt` dopo aver aggiornato i pacchetti:

```bash
apt update
apt install fuss-fucc
```

clonezilla-pxe verrà automaticamente disinstallato.

## Configurazione del cambio automatico della password di root

FUCC è in grado di modificare automaticamente la password di root dei client clonati con una criptata che gli viene passata. Per configurarlo eseguire lo script

```bash
fuss-fucc rootpw
```

ed inserire due volte la password di root da dare ai client. Di norma questo script dev'essere eseguito dopo l'installazione di fuss-fucc sul server e prima di iniziare a clonare le macchine.


## Compilazione della lista dei computer

Nella cartella "/srv/clonezilla" (normalmente cartella standard di clonezilla o altra cartella sul server FUSS che contiene la immagini da clonare) si trova il file computerList.txt in cui bisogna elencare i nomi che si vogliono assegnare ai computer specificando il mac-address, l'immagine di clonezilla che si vuole usare per il computer facendo seguire questa indicazione alla parola "join" se si vuole agganciare il computer in dominio ed eventualmente, come ultimo parametro il nome del cluster, se nel dominio si usano i cluster. 
Il file incluso nel pacchetto contiene un piccolo esempio commentato che riortiamo di seguito:

```
info-pc01 08:00:27:ab:5a:a2 cloneImage-img join clustername
```

## Primo lancio sul client

Una volta eseguito quanto sopra indicato, si avviino in "network boot (PXE)" i PC da installare (in genere si preme il tasto F12 ... ma potrebbe variare a seconda del computer). Il menu presenta due possibili scelte, "automatica" e "manuale" come indicato nello screenshot. La modalità automatica è il "default". 

![clonezilla boot](images/setup-1.png)


## Reinstallazione di un’aula con FUCC

La procedura più semplice per reinstallare un’aula con FUCC è la seguente:
1. Accendere tutti i computer e, via cssh, verificare che l’ultima versione di octofuss-client sia installata;
2. Eseguire octofuss-client per effettuare un invio manuale dei dati al server;
3. Sul server, eseguire lo script
```bash
fuss-fucc octolist NOME-IMMAGINE-CLONEZILLA;
```
4. Copiare il file computerList.txt.octo-new al posto di /srv/clonezilla/computerList.txt
5. Procedere con la reinstallazione. Ai client verrà ridato lo stesso nome di prima e lo stesso cluster.


### Immagini clonezilla 

Per le operazioni di clonazione in genere si usano le immagini standard messe a disposizione dal progetto FUSS. 
Queste immagini hanno la partizione directory radice ("/") nella SECONDA PARTIZIONE del disco fisso e pertanto nel file "script" è stata settata la variabile "rootPartition" con il valore "2" 
Se si sceglie di creare proprie immagini con uno schema di partizionamento diverso si deve indicare in suddetta variabile quale è la partizione contenente la directory radice. 
Questo deve valere per tutte le immagini che si intendono utilizzare in una determinata scuola.


## Note per lo sviluppatore sulla compilazione di FUSS-FUCC

Se non si vuole utlizzare la versione presente nel repository archive.fuss.bz.it, si può compilare autonomamente una copia di fuss-fucc.

Installare innanzitutto le dipendenze con

```bash
apt-get install -y build-essential debhelper libncurses5-dev libglib2.0-dev libgeoip-dev libtokyocabinet-dev zlib1g-dev libncursesw5-dev libbz2-dev unzip wget squashfs-tools openssh-client
```

e procedere poi alla compilazione vera e propria con:

```bash
make full-clean
make rebuild-squashfs
debian/rules binary
```

NB: i comandi vanno eseguiti come root per poter modificare i permessi dello squashfs.

