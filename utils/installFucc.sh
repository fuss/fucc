#!/bin/bash

DATUM="$(date +%d-%b-%Y-ore-%H-%M)"

# ## Set the folder of clonezilla on the fuss-server if not corresponding to the actual standard ## #

clonezillaFolder="/srv/clonezilla"

tftpFolder="/srv/tftp"

####


# Check if this version of the "squash-file" is the same. If so do nothing.

checkThisSquash="$(md5sum filesystem.squashfs | awk '{print $1}')"
checkServerSquash="$(md5sum $tftpFolder/clonezilla/live/filesystem.squashfs | awk '{print $1}')"

if [ "$checkThisSquash" != "$checkServerSquash" ];then
    mv $tftpFolder/clonezilla/live/filesystem.squashfs $tftpFolder/clonezilla/live/filesystem.squashfs.BKP-$DATUM
    cp filesystem.squashfs $tftpFolder/clonezilla/live/
fi

# ######


if [ -e /root/.ssh/authorized_keys ];then
    cp /root/.ssh/authorized_keys /root/.ssh/authorized_keys.BKP-$DATUM
fi


# Check if the SSH key already exists, if so check if is in authorized-keys
if [ -e $clonezillaFolder/.ssh/id_rsa.pub ] ; then
	if ! grep $(cat $clonezillaFolder/.ssh/id_rsa.pub|cut -d ' ' -f 2) /root/.ssh/authorized_keys 2>&1 > /dev/null ; then
		echo 'command="if [[ \"$SSH_ORIGINAL_COMMAND\" =~ ^scp[[:space:]]-f ]] || [[ \"$SSH_ORIGINAL_COMMAND\" =~ ^add_client_principal ]] || [[ \"$SSH_ORIGINAL_COMMAND\" =~ rm[[:space:]]/root/ ]]; then $SSH_ORIGINAL_COMMAND; else echo \"Access Denied $SSH_ORIGINAL_COMMAND\"; fi"' $(cat $clonezillaFolder/.ssh/id_rsa.pub) >> /root/.ssh/authorized_keys
	fi
else
	# else, generate a new keypair and send it to root's authorized keys
	mkdir -p $clonezillaFolder/.ssh
	ssh-keygen -t rsa -N "" -f $clonezillaFolder/.ssh/id_rsa -C "root@fuss-fucc"
	echo 'command="if [[ \"$SSH_ORIGINAL_COMMAND\" =~ ^scp[[:space:]]-f ]] || [[ \"$SSH_ORIGINAL_COMMAND\" =~ ^add_client_principal ]] || [[ \"$SSH_ORIGINAL_COMMAND\" =~ rm[[:space:]]/root/ ]]; then $SSH_ORIGINAL_COMMAND; else echo \"Access Denied $SSH_ORIGINAL_COMMAND\"; fi"' $(cat $clonezillaFolder/.ssh/id_rsa.pub) >> /root/.ssh/authorized_keys
fi

ssh-keyscan -H proxy > $clonezillaFolder/.ssh/known_hosts 2>/dev/null

chown -R clonezilla. $clonezillaFolder/.ssh

chmod 400 $clonezillaFolder/.ssh/id_rsa*

cp -r clientScripts $clonezillaFolder

chmod -R 770 $clonezillaFolder/clientScripts


if [ -e $tftpFolder/pxelinux.cfg/default ];then
    mv $tftpFolder/pxelinux.cfg/default $tftpFolder/pxelinux.cfg/default.BKP-$DATUM
fi

cp default $tftpFolder/pxelinux.cfg

if [ -e $clonezillaFolder/computerList.txt ];then
    mv $clonezillaFolder/computerList.txt $clonezillaFolder/computerList.txt.BKP-$DATUM
fi

cp computerList.txt $clonezillaFolder

if [ -e $clonezillaFolder/script ];then
    mv $clonezillaFolder/script $clonezillaFolder/script.BKP-$DATUM
fi

cp script $clonezillaFolder

chmod 770 $clonezillaFolder/script

chown -R clonezilla. $clonezillaFolder/*

exit 0
