#!/bin/bash

IMAGE="fuss9-64bit-2019-08-07-16-img"
LENOVO_IMAGE="001-ThincCentre-500gb-20190801-img"
JOIN="join"
DB="/var/lib/octofuss/octofuss.db"

rm -f list.txt
touch list.txt

PCS=$(sqlite3 $DB <<EOF
select distinct hostname from client_component;
.quit
EOF
)

for fqdn in $PCS; do
        client=$(echo $fqdn | cut -d '.' -f 1)
        # Check if the host has already been mapped
        if ! grep $client list.txt 2>&1 > /dev/null; then
                # Find that MAC
                MAC=$(sqlite3 $DB <<EOF
select value from client_component where hostname = "$fqdn" and component like "mac%";
.quit
EOF
)
                MAC=$(echo $MAC|cut -d ' ' -f 1)
                VENDOR=$(sqlite3 $DB <<EOF
select value from client_component where hostname = "$fqdn" and component like "system_vendor";
.quit
EOF
)
		VENDOR=$(echo $VENDOR | cut -d ' ' -f 1)
                if [ -z "$MAC" ] ; then
                        echo No mac for $fqdn
                else
			if ! grep $MAC list.txt 2>&1 > /dev/null ; then
				THIS="$IMAGE"
				if [ "$VENDOR" = "LENOVO" ] ; then
					THIS="$LENOVO_IMAGE"
				fi
	                        echo $client $MAC $THIS $JOIN $(grep $client /etc/clusters | head -1 | cut -d ' ' -f 1) >> list.txt
			else
				echo "$MAC has more than one fqdn ($fqdn)"
			fi
                fi
        fi
done

