#!/bin/bash

read -s -p "Clients password: " pw1
echo
read -s -p "Confirm clients password: " pw2
echo

if ! [ "$pw1" = "$pw2" ] ; then
	echo "Password mismatch! Please retry"
else
	perl -e "print crypt(\"$pw1\", \"salt\")" > clientScripts/new_root_pw
fi

unset pw1
unset pw2
